namespace OnlineShopping.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "Titre", c => c.String());
            AddColumn("dbo.Products", "Image", c => c.String());
            AddColumn("dbo.Products", "editeur", c => c.String());
            AddColumn("dbo.Products", "prix", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "prix");
            DropColumn("dbo.Products", "editeur");
            DropColumn("dbo.Products", "Image");
            DropColumn("dbo.Products", "Titre");
        }
    }
}
