namespace OnlineShopping.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Categories", "Titre", c => c.String());
            AddColumn("dbo.Orders", "Proprietaire", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "Proprietaire");
            DropColumn("dbo.Categories", "Titre");
        }
    }
}
