﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace OnlineShopping.Models
{
    public class Product
    {
        public int ProductId { get; set; }
        public int CategoryId { get; set; }
        public string Titre { get; set; }
        public String Image { get; set; }
        public string editeur { get; set; }
        public decimal prix { get; set; }
        public virtual Category Category { get; set; }
        public int OrderId { get; set; }
        public virtual Order Order { get; set; }
    }

   
}