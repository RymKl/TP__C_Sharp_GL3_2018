﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace OnlineShopping.Models
{
    class OnlineShoppingContext : DbContext

    {
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Pruducts { get; set; }
        public DbSet<Category> Categories { get; set; }
    }
}