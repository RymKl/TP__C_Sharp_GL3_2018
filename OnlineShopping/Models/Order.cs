﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineShopping.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public virtual ICollection<Product> Products { get; set; }
        public string Proprietaire { get; set; }
    }
}